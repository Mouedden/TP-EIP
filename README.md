# TP 5 & 6 d'Architectures Distribuées

Vous aurez à rendre ce projet en binôme pour le 19 avril 2018. 
Tout retard sera pénalisé suivant une règle de calcul exponentielle (1 point, 3 points, 6 points, etc.).

Le projet est à rendre par **email** sous la forme d'une **adresse vers un projet GITLAB privé accessible depuis le web**. 
Vous ajouterez vos encadrants (les comptes **ccaron** et **asaval**) pour qu'ils puissent avoir accès à votre projet en vous assurant que celui-ci contienne:
1. Les **noms et prénoms** de votre binôme dans un fichier **AUTEURS** à la racine
2. Le **code** intégral commenté
3. Une documentation de votre service dans un fichier **README** à la racine
4. Un **rapport** sur les problèmes rencontrés et les solutions que vous y avez apportés

Le code source du projet est disponible [ici](ZOO_MANAGER).

Vous pouvez également télécharger une version [PDF de l'énoncé du TP](TP-EIP.pdf).

**DOCUMENTATION DE NOTRE SERVICE**
1. Création d'un consommateur
// On définit un consommateur 'consumer-1'
				// qui va écrire le message
				from("direct:consumer-1").to("log:afficher-consumer-1");
				from("direct:route-1").choice().when(header("entete1").isEqualTo("toto")).to("direct:toto")
						.when(header("entete2").isEqualTo("titi")).to("direct:titi").otherwise().to("direct:tutu");
				from("direct:toto").to("log:afficher-toto");
				from("direct:titi").to("log:afficher-titi");
				from("direct:tutu").to("log:afficher-tutu");


2. Création d'un seconde consommateur
// definition de second consumer
				from("direct:messages-consumer-2").to("file:messages-consumer-2");
3. Définition d'une nouvelle route
// definition du nouvelle route
				from("direct:consumer-all").choice().when(header("entete").isEqualTo("ecrire"))
						.to("direct:messages-consumer-2").otherwise().to("direct:consumer-1");
4. Récupère les informations d'un animal demandé dans une requète
from("direct:Citymanager").setHeader(Exchange.HTTP_METHOD, constant("GET"))
						.setHeader(Exchange.HTTP_PATH).simple("/rest-service/zoo-manager/find/byName/${headers.name}")
						.to("http://localhost:8080").log("response received : ${body}");
5. Envoie d'une requète au service de Geoname
from("direct:Geoname").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/search?q=${headers.pays}&maxRows=10&style=LONG&lang=es&username=abdelkader").to("http://api.geonames.org").process(new Processor() {

							public void process(Exchange exchange) throws Exception {
								// TODO Auto-generated method stub
								String res = exchange.getIn().getBody(String.class);
								System.out.println(res);
							}
						});
				
				from("direct:Position").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
				.simple("/rest-service/zoo-manager/animals/${headers.id}/position").to("http://localhost:7791").process(new Processor() {

					public void process(Exchange exchange) throws Exception {
						// TODO Auto-generated method stub
						position = exchange.getIn().getBody(String.class);
						System.out.println(position);
						System.out.println(position.indexOf("name"));
						String pays = position.substring(position.indexOf("name") + 5);
						int indexOfFirstChevron = pays.indexOf("<");
						System.out.println(indexOfFirstChevron);
						pays = pays.substring(0, indexOfFirstChevron);
						//System.out.println(position.substring(position.indexOf("name") + , position.indexOf("name") + 3));
						System.out.println(pays);
						ProducerTemplate pt1 = context.createProducerTemplate();
						//pt1.setDefaultEndpointUri("direct:Geoname");
						pt1.sendBodyAndHeader("direct:Geoname", "g", "pays", pays);
						
					}
				});
6. Exécution de plusieurs instances de Tomcat
from("direct:instance1").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/rest-service/zoo-manager/animals").to("http://localhost:8181").to("direct:start");
				from("direct:instance2").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/rest-service/zoo-manager/animals").to("http://localhost:8282")
						.to("direct:start");
				from("direct:instance3").setHeader(Exchange.HTTP_METHOD, constant("GET")).setHeader(Exchange.HTTP_PATH)
						.simple("/rest-service/zoo-manager/animals").to("http://localhost:8383")
						.to("direct:start");

				// JOINING MESSAGE WITH AGGREGATE EIP AGGREGATE PATTERN
				from("direct:start").log("Sending ${body} with correlation key ${header.myId}")
						.aggregate(header("myId"), springContext.getBean(AggregationStrategy.class)).completionSize(3)
						.log("Sending out ${body}").to("mock:result");
			}
		};

















