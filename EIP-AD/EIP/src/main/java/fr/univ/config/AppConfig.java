package fr.univ.config;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.camel.processor.aggregate.AggregationStrategy;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import fr.univ.app.MyAggregationStrategy;


@Configuration
public class AppConfig {
	
	@Bean
	public AggregationStrategy aggregationStrategy() {
		return new MyAggregationStrategy();
	}
}
