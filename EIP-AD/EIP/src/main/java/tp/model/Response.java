package tp.model;

public class Response {
	private Object content;
	private Object position;
	public Object getContent() {
		return content;
	}
	public void setContent(Object content) {
		this.content = content;
	}
	public Object getPosition() {
		return position;
	}
	public void setPosition(Object position) {
		this.position = position;
	}
	
	
}
